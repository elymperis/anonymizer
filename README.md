Getmap modified repo: https://gitlab.com/elymperis/anonymizer

> Forked from https://github.com/understand-ai/anonymizer

You will need to install:

- Anaconda 2024.02
- An anaconda virtual environment with **Python3.6**
  (Try with these first, and if failing, also install MS Visual C++)
- MS Visual C++ 2014 (or later) , required by numpy (Build Tools)
  You can find v2022: https://aka.ms/vs/17/release/vs_BuildTools.exe or https://visualstudio.microsoft.com/visual-cpp-build-tools/
- Optionally CUDA, if intending to run on a GPU (https://developer.nvidia.com/cuda-downloads)

# Installation - Getmap /w Anaconda

1. Install anaconda, and create an environment with Python 3.6 (e.g. named _anonymizer_)
2. Open the _Anaconda Prompt_, and run `conda activate anonymizer`
3. cd to this project's directory `cd /path/to/anonymizer`
4. run `python -m pip install -r requirements.txt`
5. Download the model weights and add them to a folder of your choice

# Anonymize

### Single process

```
cd /path/to/anonymizer
conda activate anonymizer
python main.py --input /path/to/input --image-output /path/to/output  --weights .weights --obfuscation-kernel 23,3,7 --no-write-detections --face-threshold 0.05 --plate-threshold 0.07
```

### Multiple processes
> Note: DON'T use a subfolder of the input folder as the output folder
You can also run multiple processes in parallel using `multi.py` (uses child shell processes, **prefered**).
To do so, you call `python multi.py --config config.json`. The config file contains the execution params for multiple inputs/outputs.
Be careful, on windows, the paths defined in config.json should have their backslashes escaped!

### Conda setup

1. Find conda installation path, and add the following env variables to Path
   `...\anaconda3\Library\bin`
   ` ...\anaconda3\Scripts`
2. Create a conda environment:

```bash
conda create -n anonymizer python=3.6
```

3. Navigate to this folder (_anonymizer_) and run:

```bash
conda init
conda activate anonymizer
```

---

---

⚠️ **ARCHIVED REPOSITORY** ⚠️

**We decided to archive this repository to make it read-only and indicate that it's no longer actively maintained.**

---

# understand.ai Anonymizer [ARCHIVED]

To improve privacy and make it easier for companies to comply with GDPR, we at [understand.ai](https://understand.ai/) decided to open-source our anonymization software and weights for a model trained on our in-house datasets for faces and license plates.
The model is trained with the [Tensorflow Object Detection API](https://github.com/tensorflow/models/tree/master/research/object_detection) to make it easy for everyone to use these weights in their projects.

Our anonymizer is used for projects with some of Germany's largest car manufacturers and suppliers,
but we are sure there are many more applications.

## Disclaimer

Please note that the version here is not identical to the anonymizer we use in customer projects. This model is an early version in terms of quality and speed. The code is written for easy-of-use instead of speed.  
For this reason, no multiprocessing code or batched detection and blurring are used in this repository.

This version of our anonymizer is trained to detect faces and license plates in images recorded with sensors
typically used in autonomous vehicles. It will not work on low-quality or grayscale images and will also not work on
fish-eye or other extreme camera configurations.

## Examples

![License Plate Example Raw](images/coco02.jpg?raw=true "Title")
![License Plate Anonymized](images/coco02_anonymized.jpg?raw=true "Title")

![Face Example Raw](images/coco01.jpg?raw=true "Title")
![Face Example Anonymized](images/coco01_anonymized.jpg?raw=true "Title")

## Installation

To install the anonymizer just clone this repository, create a new python3.6 environment and install the dependencies.  
The sequence of commands to do all this is

```bash
python -m venv ~/.virtualenvs/anonymizer
source ~/.virtualenvs/anonymizer/bin/activate

git clone https://github.com/understand-ai/anonymizer
cd anonymizer

pip install --upgrade pip
pip install -r requirements.txt
```

To make sure everything is working as intended run the test suite with the following command

```bash
pytest
```

Running the test cases can take several minutes and is dependent on your GPU (or CPU) and internet speed.  
Some test cases download model weights and some perform inference to make sure everything works as intended.

## Weights

[weights_face_v1.0.0.pb](https://drive.google.com/file/d/1CwChAYxJo3mON6rcvXsl82FMSKj82vxF)

[weights_plate_v1.0.0.pb](https://drive.google.com/file/d/1Fls9FYlQdRlLAtw-GVS_ie1oQUYmci9g)

## Usage

In case you want to run the model on CPU, make sure that you install `tensorflow` instead of `tensorflow-gpu` listed
in the `requirements.txt`.

Since the weights will be downloaded automatically all that is needed to anonymize images is to run

```bash
PYTHONPATH=$PYTHONPATH:. python anonymizer/bin/anonymize.py --input /path/to/input_folder --image-output /path/to/output_folder --weights /path/to/store/weights
```

from the top folder of this repository. This will save both anonymized images and detection results as json-files to
the output folder.

### Advanced Usage

In case you do not want to save the detections to json, add the parameter `no-write-detections`.
Example:

```bash
PYTHONPATH=$PYTHONPATH:. python anonymizer/bin/anonymize.py --input /path/to/input_folder --image-output /path/to/output_folder --weights /path/to/store/weights --no-write-detections
```

Detection threshold for faces and license plates can be passed as additional parameters.
Both are floats in [0.001, 1.0]. Example:

```bash
PYTHONPATH=$PYTHONPATH:. python anonymizer/bin/anonymize.py --input /path/to/input_folder --image-output /path/to/output_folder --weights /path/to/store/weights --face-threshold=0.1 --plate-threshold=0.9
```

By default only `*.jpg` and `*.png` files are anonymized. To for instance only anonymize jpgs and tiffs,
the parameter `image-extensions` can be used. Example:

```bash
PYTHONPATH=$PYTHONPATH:. python anonymizer/bin/anonymize.py --input /path/to/input_folder --image-output /path/to/output_folder --weights /path/to/store/weights --image-extensions=jpg,tiff
```

The parameters for the blurring can be changed as well. For this the parameter `obfuscation-kernel` is used.
It consists of three values: The size of the gaussian kernel used for blurring, it's standard deviation and the size
of another kernel that is used to make the transition between blurred and non-blurred regions smoother.
Example usage:

```bash
PYTHONPATH=$PYTHONPATH:. python anonymizer/bin/anonymize.py --input /path/to/input_folder --image-output /path/to/output_folder --weights /path/to/store/weights --obfuscation-kernel="65,3,19"
```

## Attributions

An image for one of the test cases was taken from the COCO dataset.  
The pictures in this README are under an [Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/legalcode) license.
You can find the pictures [here](http://farm4.staticflickr.com/3081/2289618559_2daf30a365_z.jpg) and [here](http://farm8.staticflickr.com/7062/6802736606_ed325d0452_z.jpg).
