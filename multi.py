"""
Runs anonymizer in parallel, using sub-shells on Windows. The configuration file must be a JSON file with a list of
dictionaries, each containing the parameters for a single instance of main.py. The parameters are the same as the
ones accepted by main.py.
"""

import argparse
import json
import os

def parse_args():
    parser = argparse.ArgumentParser(description="Run multiple instances of main.py in separate command windows.")
    parser.add_argument('--config', required=True, help='Path to the JSON configuration file.')
    return parser.parse_args()

def run_instance(params):
    
    # check platform 
    if os.name == 'nt':
        command = f"start cmd.exe /k python main.py --input {params['input']} --image-output {params['image_output']} --weights {params['weights']} --image-extensions {params['image_extensions']} --face-threshold {params['face_threshold']} --plate-threshold {params['plate_threshold']} {'--resume' if params['resume']==True else ''} --obfuscation-kernel {params['obfuscation_kernel']} "

    else:
        print("This script is only supported on Windows.") 
        return
        
    if params.get('write_detections'):
        command += " --write-detections"
    else:
        command += " --no-write-detections"

    os.system(command)

def main():
    args = parse_args()
    with open(args.config, 'r') as file:
        config = json.load(file)

    for instance_params in config:
        run_instance(instance_params)

if __name__ == "__main__":
    main()
