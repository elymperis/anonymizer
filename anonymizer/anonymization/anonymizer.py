import json
from pathlib import Path

import numpy as np
from PIL import Image
from tqdm import tqdm

from time import perf_counter
from datetime import datetime
import logging

log = logging.getLogger(__name__)

def load_np_image(image_path):
    image = Image.open(image_path).convert('RGB')
    np_image = np.array(image)
    metadata = image.info.get('exif', b'')
    
    return np_image, metadata


def save_np_image(image, image_path, metadata):
    pil_image = Image.fromarray((image).astype(np.uint8), mode='RGB')
    
    # Save with metadata
    if metadata:
        pil_image.save(
            image_path,
            quality=95,
            exif=metadata
        )
    
    else:
        pil_image.save(
            image_path,
            quality=95
        )


def save_detections(detections, detections_path):
    json_output = []
    for box in detections:
        json_output.append({
            'y_min': box.y_min,
            'x_min': box.x_min,
            'y_max': box.y_max,
            'x_max': box.x_max,
            'score': box.score,
            'kind': box.kind
        })
    with open(detections_path, 'w') as output_file:
        json.dump(json_output, output_file, indent=2)


class Anonymizer:
    def __init__(self, detectors, obfuscator, resume=False):
        self.detectors = detectors
        self.obfuscator = obfuscator
        self.resume = resume

    def check_resume(self, input_path, output_path, file_type):

            original_files = list(Path(input_path).glob(f'**/*.{file_type}'))
            
            if len(original_files) == 0:
                return []
                    
            anonymized_files = list(Path(output_path).glob(f'**/*.{file_type}'))
            
            _input_count = len(original_files)
            _anonymized_count = len(anonymized_files)
            _remaining_count = _input_count - _anonymized_count
            _remaining_percent = ((_remaining_count / len(original_files)) * 100) if len(original_files) > 0 else 0
            _completed_percent = 100 - _remaining_percent
            
            log.info("---- RESUMING ANONYMIZATION ({file_type}) ----")
            try:
                log.info(f"-✅  {_anonymized_count} ({file_type}) files completed ({_completed_percent:.2f}%)")
                log.info(f"-❌  {_remaining_count} ({file_type}) files remaining ({_remaining_percent:.2f}%)")
            except Exception as e:
                log.info(f"- {_anonymized_count} ({file_type}) files completed ({_completed_percent:.2f}%)")
                log.info(f"- {_remaining_count} ({file_type}) files remaining ({_remaining_percent:.2f}%)")
    
            return [file.name for file in anonymized_files]
         


    def anonymize_image(self, image, detection_thresholds):
        assert set(self.detectors.keys()) == set(detection_thresholds.keys()),\
            'Detector names must match detection threshold names'
        detected_boxes = []
        for kind, detector in self.detectors.items():
            new_boxes = detector.detect(image, detection_threshold=detection_thresholds[kind])
            detected_boxes.extend(new_boxes)
        return self.obfuscator.obfuscate(image, detected_boxes), detected_boxes

    def anonymize_images(self, input_path, output_path, detection_thresholds, file_types, write_json):
        
        t0 = perf_counter()
        
        Path(output_path).mkdir(exist_ok=True)
        assert Path(output_path).is_dir(), 'Output path must be a directory'

        log_path = Path(output_path, f"log_{datetime.now().strftime('%Y-%m-%d-%H-%M-%S')}.log")
        log.addHandler(logging.FileHandler(log_path))
        log.addHandler(logging.StreamHandler())
        log.level = logging.INFO


        # Log anonymization parameters
        log.info(f'Input: {input_path} \nOutput: {output_path}...')
        log.info("Detection thresholds: %s", detection_thresholds)
        log.info(f"Obfuscation params: {self.obfuscator.kernel_size}, {self.obfuscator.sigma}, {self.obfuscator.box_kernel_size}")
        
        try:
            log.info(f"Resume option: {'✅' if self.resume else '❌ (starting from scratch)'}")
        except Exception as e:
            log.info(f"Resume option: {'True' if self.resume else 'False (starting from scratch)'}")

            

        files = []
        for file_type in file_types:
            files_to_anonymize = list(Path(input_path).glob(f'**/*.{file_type}'))
            
            if self.resume:
                exclude_files = self.check_resume(input_path, output_path, file_type)
                files_to_anonymize = [file for file in files_to_anonymize if file.name not in exclude_files]
                
            files.extend(files_to_anonymize)


        img_count = 0
        for input_image_path in tqdm(files):
            # Create output directory
            relative_path = input_image_path.relative_to(input_path)
            (Path(output_path) / relative_path.parent).mkdir(exist_ok=True, parents=True)
            output_image_path = Path(output_path) / relative_path
            output_detections_path = (Path(output_path) / relative_path).with_suffix('.json')

            # Anonymize image
            image, metadata = load_np_image(str(input_image_path))
            anonymized_image, detections = self.anonymize_image(image=image, detection_thresholds=detection_thresholds)
            save_np_image(image=anonymized_image, image_path=str(output_image_path), metadata=metadata)
            if write_json:
                save_detections(detections=detections, detections_path=str(output_detections_path))
            
            img_count += 1


        t_end = perf_counter() - t0
        mean_img_t = t_end/img_count if img_count > 0 else 0
    
        log.info("🙏 Anonymized %d images", img_count)
        log.info("Mean anonymization time per image: %.2f seconds", mean_img_t)
        log.info("Anonymization took %.2f seconds", t_end)
